module.exports = function(grunt) {

	// Project configuration.
	grunt.initConfig({
		pkg: grunt.file.readJSON('package.json'),
		compass: {                  // Task
			dev: {
				options: {              // Target options
					sassDir: 'sass',
					cssDir: 'docroot/css',
					environment: 'development',
					relativeAssets: true,
					raw: 'preferred_syntax = :sass\n',
					require: 'susy'
				}
			}
		},
		watch: {
			compass: {
				files: ['**/*.sass'],
				tasks: ['compass'],
			},
		},
	});

	// Load all plugins.
	grunt.loadNpmTasks('grunt-contrib-compass');
	grunt.loadNpmTasks('grunt-contrib-watch');

	// Default task(s).
	grunt.registerTask('default', ['watch']);
};