// Get all the range input-elements
var rangeInputs = document.querySelectorAll('.sliders input');

// Init the values for box-shadow's properties
// and get the demo-element which will use
// the box-shadow
var hShadowInput = document.querySelectorAll('.js-h-shadow input');
var hShadowValue = hShadowInput[0].value;

var vShadowInput = document.querySelectorAll('.js-v-shadow input');
var vShadowValue = vShadowInput[0].value;

var blurInput = document.querySelectorAll('.js-blur input');
var blurValue = blurInput[0].value;

var spreadInput = document.querySelectorAll('.js-spread input');
var spreadValue = spreadInput[0].value;

var demoBox = document.getElementsByClassName('js-target');

// Loop through each range input-elements
Array.prototype.forEach.call(rangeInputs, function(el, i){

  // When the input is dragged, set the corresponding
  // variables value to input's value
  el.oninput = function(){
    switch(this.name) {
      case "h-shadow":
        hShadowValue = this.value;
        break;
      case "v-shadow":
        vShadowValue = this.value;
        break;
      case "blur":
        blurValue = this.value;
        break;
      case "spread":
        spreadValue = this.value;
        break;
      default:
        break;
    }

    // Update the box-shadow to the demo-element
    demoBox[0].style.boxShadow = hShadowValue + "px " + vShadowValue + "px " + blurValue + "px " + spreadValue + "px #000";
  };
});