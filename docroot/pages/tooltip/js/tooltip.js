// Get all the range input-elements
var rangeInputs = document.querySelectorAll('.sliders input');

// Init the values for border radius's properties
// and get the demo-element which will use
// the border radius
var brLeftTop      = document.querySelectorAll('.js-br-left-top input');
var brLeftTopValue = brLeftTop[0].value;

var brRightTop      = document.querySelectorAll('.js-br-right-top input');
var brRightTopValue = brRightTop[0].value;

var brRightBottom      = document.querySelectorAll('.js-br-right-bottom input');
var brRightBottomValue = brRightBottom[0].value;

var brLeftBottom      = document.querySelectorAll('.js-br-left-bottom input');
var brLeftBottomValue = brLeftBottom[0].value;

var demoBox = document.getElementsByClassName('js-target');

// Loop through each range input-elements
Array.prototype.forEach.call(rangeInputs, function(el, i){

  // When the input is dragged, set the corresponding
  // variables value to input's value
  el.oninput = function(){
    switch(this.name) {
      case "br-left-top":
        brLeftTopValue = this.value;
        break;
      case "br-right-top":
        brRightTopValue = this.value;
        break;
      case "br-right-bottom":
        brRightBottomValue = this.value;
        break;
      case "br-left-bottom":
        brLeftBottomValue = this.value;
        break;
      default:
        break;
    }

    // Update the border-radius to the demo-element
    demoBox[0].style.borderTopLeftRadius     = brLeftTopValue + "px ";
    demoBox[0].style.borderTopRightRadius    = brRightTopValue + "px ";
    demoBox[0].style.borderBottomRightRadius = brRightBottomValue + "px ";
    demoBox[0].style.borderBottomLeftRadius  = brLeftBottomValue + "px ";
  };
});